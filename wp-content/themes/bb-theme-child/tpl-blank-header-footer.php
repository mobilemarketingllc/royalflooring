<?php

/*
Template Name: Blank Header/Footer
Template Post Type: post, page
*/

 ?>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<meta property="fb:app_id" content="424426844274364"/>

<meta property="og:url" content="https://shoproyalflooring.com/sweepstakes"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="Royal Flooring Teacher's Carpet Square Giveaway"/>

<meta property="og:site_name" content="Royal Flooring Teacher's Carpet Square Giveaway"/>
<meta property="fb:admins" content="1088490351"/>
<meta property="fb:admins" content="11512735"/>
<meta property="fb:admins" content="1088490312"/>
<meta property="og:description" content="This August we're giving away FREE carpet squares to teachers in the Des Moines area. Simply fill out the form below to register and a representative from our staff will contact you with pick-up details. (Available while supplies last.)"/>
<title>Royal Flooring Teacher's Carpet Square Giveaway</title>
<style type="text/css">
body { border: 0; margin: 0; padding: 0;min-height:640px; }
</style>

<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />

</head>
<body style="margin:0px;padding:0px;">

</body>
</html>
